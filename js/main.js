// подключаем конфигурационный файл связи с сайтом нетологии
var authModuleNetology = require('../netology.config.js')

var MagicBockapp = angular.module('MagicBockapp', ['lightboock', require('angular-sanitize'), require('angular-route')])
//собираем страницу
MagicBockapp.config(function($routeProvider, $locationProvider){
  $routeProvider
    .when('/', {
      templateUrl:'./space/space_main/space_main.html',
      controller:'LessinBoock',
      controllerAs:'LessinBoock'
    })
    .when('/space_book/:bookId', {
      templateUrl:'./space/space_book/space_book.html',
      controller:'BookOne',
      controllerAs:'BookOne'
    })
    .when('/space_compani', {
      templateUrl:'./space/space_compani/space_compani.html'
    })
    .when('/space_order/:bookId', {
      templateUrl:'./space/space_order/space_order.html',
      controller:'Order',
      controllerAs:'Order'
    })

  $locationProvider.html5Mode(true);
})

//Моргающий глаз, вычисление координат.
MagicBockapp.directive('ngEye', function() {
  function link(scope, element, attrs) {

    var took = element.children()[0];
    var tookWidth = took.clientWidth;
    var tookHeight = took.clientHeight;

    var eyeApple = took.querySelector('.apple-mergin');
    var eyeAppleWidth = eyeApple.clientWidth;
    var eyeAppleHeight = eyeApple.clientHeight;

    var centerH = window.innerWidth / 2;
    var centerV = window.innerHeight / 2;

    document.addEventListener('mousemove', function(e) {
      var moveX = -(e.pageX - centerH) / 30;
      var moveY = -(e.pageY - centerV) / 30;

      var top = (tookHeight / 2) - (eyeAppleHeight / 2) - moveY +'px';
      var left = (tookWidth / 2) - (eyeAppleWidth / 2) - moveX +'px';
      scope.Styleappl = { 'top': top, 'left': left }
      scope.$apply();
    })
  }
  return {
    templateUrl: './space/magicstyles/magicstyles.html',
    link: link
  }
})
//при нажатии кнопки выбрасываем по 4-ре книги
MagicBockapp.controller('LessinBoock', function(Magicbock, $routeParams) {
  var limitBooks = 4;
  this.books = Magicbock.query();
  this.limitBooks = limitBooks;
  this.showBtn = true;
  this.upLimitBooks = function() {
    this.limitBooks += limitBooks;
    this.showBtn = !(this.limitBooks >= this.books.length);
  }

})

MagicBockapp.controller('BookOne', function(Magicbock, $routeParams) {
  this.book = Magicbock.get({bookId: $routeParams.bookId});
})

MagicBockapp.controller('Order', function(Magicbock, OrderMethods, $routeParams, $http) {
//параметры заказа
  var deliveryPrice = 0;
  this.book = Magicbock.get({bookId: $routeParams.bookId});
  this.deliveries = OrderMethods.getDelivery();
  this.payments = OrderMethods.getPayment();
  this.needAdress = false;

  this.addressCheck = function(obj) {
    this.needAdress = obj.needAdress;
  }

  this.getDeliveryName = function(obj) {
    var result = '';

    if( obj.price === 0 ) result = obj.name + ' - Бесплатно';
    else result = obj.name + ' - ' + obj.price + ' Z';

    return result;
  }

  this.getTotal = function(id) {
    var price,
        obj = this.deliveries.find(function(el) { return el.id === id });
    
    if(!obj) return 'Рассчитываем цену';
    
    price = this.book.price + obj.price;

    return 'Итого к оплате: ' + price + ' Z';
  }
  //отправка формы

  this.form = {
    name: '',
    phone: '',
    email: '',
    comment: '',
    delivery: '',
    address: '',
    paymentId: ''
  }

  this.submitOrder = function() {
    this.error = false;

    var config = {
      method: 'POST',
      url: 'https://netology-fbb-store-api.herokuapp.com/order',
      data: {
        book: this.book.id,
        name: this.form.name,
        phone: this.form.phone,
        email: this.form.email,
        comment: this.form.comment,
        delivery: {
        id: this.form.delivery,
        address: this.form.address
        },
        payment: {
          id: this.form.paymentId,
          currency: this.book.currency
        },
        manager: 'tinirait@gmail.com',
      }
    }

    $http(config)
      .then(this.handleSuccess.bind(this), this.handleError.bind(this))
  }

  this.handleSuccess = function() {
    this.success = true;
  }

  this.handleError = function(response) {
    this.error = true;
    this.errorMsg = response.data.message;
  }

})