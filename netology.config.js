var angular = require('angular');

angular.module('lightboock', [require('angular-resource')])

  .factory('OrderMethods', function($resource){
    return $resource('https://netology-fbb-store-api.herokuapp.com/order/:methodOrder', {}, {
      getDelivery: {method:'GET', params:{methodOrder:'delivery'}, isArray:true},
      getPayment: {method:'GET', params:{methodOrder:'payment'}, isArray:true},
    });
  })
   .factory('Magicbock', function($resource){
    return $resource('https://netology-fbb-store-api.herokuapp.com/book/:bookId', {}, {
      query: {method:'GET', params:{bookId:''}, isArray:true}
    });
  })

