// С помощью webpack производим сборку мудуля js
module.exports = {
  output: {
    path: __dirname,
    filename: './site/common.js'
  },
  entry: './js/main.js',
  resolve: {
    extensions: ['', '.js']
  },
};